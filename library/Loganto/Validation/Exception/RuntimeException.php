<?php

namespace Loganto\Validation\Exception;

class RuntimeException extends \RuntimeException implements \Loganto\Validation\Exception\ExceptionInterface
{
}
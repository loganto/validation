<?php

namespace Loganto;

class Validation {
	private $errors;
	private $messages;
	private $fields;
	private $sets;
	private $rules;
	private $callbacks;
	private $human;
	private $param;
	
    public function __construct() {
		$this->reset();
    }
	
	/**
	 * This reset the instance so we can start new validation
	 * 
	 * @return $this - to allow fluid interface
	 */
	public function reset() {
		$this->errors = array();
		$this->fields = array();
		$this->sets = array();
		$this->rules = array();
		$this->callbacks = array();
		$this->human = array();
		$this->param = null;
		$this->messages = array(
			'required' => 'required',
			'valNotEmpty' => 'cannot be empty',
			'valEmail' => 'invalid email',
			'valInt' => 'integer value required',
			'valDecimal' => 'decimal value required',
			'valDecimalOrInteger' => 'number required',
			'valStr' => 'string value required',
			'valAlpha' => 'only alpha charachters are allowed',
			'valArray' => 'incorrect value received',
			'valRegex' => 'incorrect value received',
			'valAlphaNum' => 'only alphanumeric charachters are allowed',
			'valNumeric' => 'numeric value expected',
			'valNaturalNoZero' => 'natural value expected (zero excluded)',
			'valPos' => 'positive number required',
			'valNeg' => 'negative number required',
			'valBool' => 'boolean value required',
			'valIp' => 'invalid ip',
			'valUrl' => 'invalid url',
			'valMinStrLength' => 'failed minimal length requirement.',
			'valMaxStrLength' => 'failed maximal length requirement.',
			'valExactStrLength' => 'failed exact length requirement.',
			'valMatches' => 'value is not what was expected',
			'valNumGreaterThan' => 'outside of allowed range',
			'valNumLessThan' => 'outside of allowed range',
			'valNumEqualTo' => 'invalid value',
			'valTimezone' => 'invalid timezone',
			'valStrInRange' => 'invalid string',
			'mustExist' => 'incomplete data'
		);
		
		return $this;
	}
	
	/**
	 * Add rules to be run on specific variables
	 * 
	 * @param string $name - name of the variable for which these rules are designated
	 * @param string|array $rules - validation rules for that variable
	 * @return $this - to allow for fluid interface
	 */
	public function addRule( $name = null, $rules = null ) {
		if ( $name === null || !is_string( $name ) ) {
			throw new Exception\RuntimeException( 'Cannot add a rule - rule name is needed' );
		}
		
		$ruleset = array();
		if ( is_string( $rules ) && ( $rules !== '' ) ) {
			$rules = explode( '|', $rules );
            foreach ( $rules as $rule ) {
                $ruleset[] = ( preg_match( "/(.*?)\[(.*)\]/", $rule, $match ) ) ? array( $match[1], $match[2] ) : array( $rule, null );
            }
		} elseif ( is_array( $rules ) ) {
            foreach ( $rules as $rule ) {
				if ( is_array( $rule ) && ( count( $rule ) >= 1 ) && ( count( $rule ) <= 2 ) ) {
					$ruleset[] = ( count( $rule ) == 1 ) ? array( $rule[0], null ) : array( $rule[0], $rule[1] );
				} else if( is_string( $rule ) && ( $rule !== '' ) ) {
					$ruleset[] = ( preg_match( "/(.*?)\[(.*)\]/", $rule, $match ) ) ? array( $match[1], $match[2] ) : array( $rule, null );
				} else {
					throw new Exception\RuntimeException( 'Cannot add rule - invalid format' );
				}
            }
		} else {
			throw new Exception\RuntimeException( 'Cannot add ruleset - ruleset list must be either non-empty string or non-empty array' );
		}
		
        /*
         * At this point we've got universal ruleset that we should append to rules.
         * Same rule will not be owerwritten, butwill be repeated - this is design choice.
         */
		$this->rules[ $name ] = ( array_key_exists( $name, $this->rules ) ) ? array_merge( $this->rules[ $name ], $ruleset ) : $ruleset;
		return $this;
	}
	
	/**
	 * Setting rules in bulk while overwriting old ones
	 * 
	 * @param array rules - bulk of rules as KV array
	 * @return $this - to allow for fluid interface
	 */
	public function setRules( array $rules ) {
		$this->rules = array();
		foreach ( $rules as $k => $v ) {
			$this->addRule( $k, $v );
		}
		return $this;
	}

	/**
	 * Adding rules in bulk while keeping old ones
	 * 
	 * @param array rules - bulk of rules as KV array
	 * @return $this - to allow for fluid interface
	 */
	public function addRules( array $rules ) {
		foreach ( $rules as $k => $v ) {
			$this->addRule( $k, $v );
		}
		return $this;
	}

	/**
	 * Getting rules
	 * 
	 * @returns array rules - returns current set of rules
	 */
	public function getRules( ) {
		return $this->rules;
	}
	
	/**
	 * Add callback function
	 *
	 * @param function - anonymous function
	 * @param name - name of the callback (cannot use method names here)
	 * @return $this - to allow for fluid interface
	 */
	public function addCallback( $name, $function ) {
		$this->callbacks[ $name ] = $function;
		return $this;
	}

	/**
	 * Add callback functions in bulk without overwriting old ones
	 * 
	 * @param callbacks - array of callbacks
	 * @return this - to allow for fluid interface
	 */
	public function addCallbacks( array $callbacks ) {
		foreach( $callbacks as $key => $value ) {
			$this->callbacks[ $key ] = $value;
		}
		return $this;
	}
	
	/**
	 * Set callback functions in bulk, while overwriting old ones
	 * 
	 * @param callbacks - array of callbacks
	 * @return this - to allow for fluid interface
	 */
	public function setCallbacks( array $callbacks ) {
		$this->callbacks = array();
		foreach( $callbacks as $key => $value ) {
			$this->callbacks[ $key ] = $value;
		}
		return $this;
	}
	
	/**
	 * Get callbacks
	 * 
	 * @return callbacks - array of callback
	 */
	public function getCallbacks() {
		return $this->callbacks;
	}
	
	/**
	 * Adding variables to be validated. Values can be added directly as ex: $_POST or $_GET
     * or for human name to be specified - set optional $human param to true.
     * All existing values in fields are kept.
	 * 
	 * @param array $fields - array of fields to check
	 * @return $this - to allow for fluid interface
	 */
	public function addFields( array $fields, $human=false ) {
        if ( $human ) {
            foreach( $fields as $key => $value ) {
                if ( count( $value ) !== 3  ) {
			        throw new Exception\RuntimeException( "When setting fields with human labels - array with 3 values is expected" );
                }
                $this->addField( $value[0], $value[1], $value[2] );
            } 
        } else {
            foreach( $fields as $key => $value ) {
                $this->fields[ $key ] = $value;
            }
        }
		return $this;
	}
	
	/**
	 * Setting variables to be validated. Values can be added directly as ex: $_POST or $_GET
     * or for human name to be specified - set optional $human param to true.
     * All existing values in fields are erased.
	 * 
	 * @param array $fields - array of fields to check
	 * @return $this - to allow for fluid interface
	 */
	public function setFields( array $fields, $human=false ) {
		$this->fields = array();
        if ( $human ) {
            foreach( $fields as $key => $value ) {
                if ( count( $value ) !== 3  ) {
					throw new Exception\RuntimeException( "When setting fields with human labels - array with 3 values is expected" );
                }
                $this->addField( $value[0], $value[1], $value[2] );
            } 
        } else {
            foreach( $fields as $key => $value ) {
                $this->fields[ $key ] = $value;
            }
        }
		return $this;
	}
	
	/**
	 * Getting all fields - validated and not. 
	 */
	public function getFields() {
		return $this->fields;
	}

	/**
	 * Adding single variable to be validated
	 * 
	 * @param string name - variable name
	 * @param string value - variable value
	 * @param string human - optional human name for a variable
	 * @returns $this - to allow for fluid interface
	 */
	public function addField( $name, $value, $human=null ) {
		$this->fields[ $name ] = $value;
		$this->human[ $name ] = $human;
		return $this;
	}
	
	/**
	 * Returns only items that passed validation
	 * 
	 * @return array $this->fields
	 */
	public function getFiltered() {
		return array_diff( $this->fields, $this->getErrors() );
	}
	
	/**
	 * Returns single filtered and sanitized variable
	 * 
     * @param string $var - name of the variable to retrieve
	 * @returns mixed
	 */
	public function getFilteredSingle( $var ) {
		if ( !array_key_exists($var, $this->fields) ) {
	        throw new Exception\RuntimeException( "Unable to find a required single validated value" );
		}
		if ( array_key_exists( $var, $this->getErrors() ) ) {
	        throw new Exception\RuntimeException( "Value haven't passed validation." );
		}
		return $this->fields[ $var ];
	}
	
	/**
	 * Returns validated data from specific set (must use copyToSet[setName::optionalType] in rules)
	 * 
     * @param string $setName - name of the set
	 * @returns array
	 */
	public function getFilteredSet( $setName ) {
		if ( !array_key_exists( $setName, $this->sets ) ) {
			return array();
		}
		
		return array_diff( $this->sets[$setName], $this->getErrors() );
	}
	
	/**
	 * Add message
	 *
	 * @param function - anonymous function
	 * @param name - name of the rule or a callback
	 * @return $this - to allow for fluid interface
	 */
	public function addMessage( $name, $message ) {
		$this->messages[ $name ] = $message;
		return $this;
	}

	/**
	 * Add mesages in bulk without wiping out all old ones
	 * 
	 * @param messages - array of callbacks
	 * @return this - to allow for fluid interface
	 */
	public function addMessages( array $messages ) {
		foreach( $messages as $key => $value ) {
			$this->messages[ $key ] = $value;
		}
		return $this;
	}
	
	/**
	 * Set mesages in bulk while erasing all old ones
	 * 
	 * @param messages - array of callbacks
	 * @return this - to allow for fluid interface
	 */
	public function setMessages( array $messages ) {
		$this->messages = array();
		foreach( $messages as $key => $value ) {
			$this->messages[ $key ] = $value;
		}
		return $this;
	}
	
	/**
	 * Get messages
	 * 
	 * @return messages - array of messages
	 */
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get errors set after validation
	 * 
	 * @returns array
	 */
	public function getErrors() {
		return $this->errors;
	}
	
	/**
	 * Set human name
	 * 
	 * @param string $varName - variable name
	 * @param string $humanName - human name
	 * 
	 * @return this - to allow for fluid interface
	 */
	public function setHumanName($varName, $humanName) {
		if (!is_string($varName) || !is_string($humanName)) {
			throw new Exception\RuntimeException("Variable and human names should be strings");
		}
		$this->human[$varName] = $humanName;
		
		return $this;
	}
	
	/**
	 * Set human names
	 * 
	 * @param array $names - array of variable to human names keypairs
	 * 
	 * @return this - to allow for fluid interface
	 */
	public function setHumanNames($names) {
		foreach($names as $k => $v) {
			$this->setHumanName($k, $v);
		}
		
		return $this;
	}
	
	/**
	 * Add validation error
	 * 
	 * @returns array
	 */
    private function addError( $field, $error, $param = null ) {
        /*
         * See if validation result array is set yet
         */
		if ( !array_key_exists( $field, $this->errors ) ) {
			$this->errors[ $field ] = array();
		}

        /*
         * getting name/human name and passed in params
         */
        $firstS = isset( $this->human[ $field ] ) ? $this->human[ $field ] : $field;
        $secondS = isset( $this->param ) ? $this->param : '';
        $err = sprintf( $error, $firstS, $secondS );
        $this->errors[ $field ][ $err ] = $firstS;
    }


	/**
	 * Initiates validation
	 * 
	 * @returns boolean - FALSE on failed, TRUE on passed
	 */
	public function run() {
		/*
		 * Removing all extra variables ( the ones not present in rules but present in fields )
		 */
		$ruleNames= array_keys( $this->rules );
		foreach( $this->fields as $fieldName => $fieldValue ) {
			if ( !in_array( $fieldName, $ruleNames ) ) {
				unset( $this->fields[$fieldName] );
			}
		}

		/*
		 * Iterate over rules list and run checks
		 */
		foreach( $this->rules as $varName => $varRules ) {
			/*
			 * Find if required is set (removing all required from rules as well)
			 */
            $required = false;
			$parseOnlyIfPresent = false;
			$parseOnlyIfPresentAndNonEmpty = false;
            foreach ( $varRules as $k1 => $v1 ) {
                if ( $v1[0] == 'required' ) {
                    $required = true;
					unset( $varRules[ $k1 ] );
                } else if ( $v1[0] == 'parseOnlyIfPresent' ) {
                    $parseOnlyIfPresent = true;
					unset( $varRules[ $k1 ] );
                } else if ( $v1[0] == 'parseOnlyIfPresentAndNonEmpty' ) {
                    $parseOnlyIfPresentAndNonEmpty = true;
					unset( $varRules[ $k1 ] );
                }
				
            }
            
            /*
             * if required - check value presence
             */
            if ( !array_key_exists( $varName, $this->fields ) ) {
                if ( $required ) {
                    $err = isset( $this->messages['required'] ) ? $this->messages['required'] : _( 'validation failed' );
                    $this->addError( $varName, $err );
                }
                continue; 
            }
			
			/*
			 * If $parseOnlyIfPresent set and no value found - continue without errors
			 */
            if ( !array_key_exists( $varName, $this->fields ) && $parseOnlyIfPresent ) {
                continue; 
            }
			
			/*
			 * If $parseOnlyIfPresentAndNonEmpty set and value found but empty - continue without errors
			 */
            if ( array_key_exists( $varName, $this->fields ) && empty( $this->fields[$varName] ) && $parseOnlyIfPresentAndNonEmpty ) {
                continue; 
            }
			
			/*
			 * Checking each rule
			 */
			reset( $varRules );
			foreach ( $varRules as $num => $ruleArray ) {
                $rule = $ruleArray[0];
                $param = $ruleArray[1];
                $this->param = $param;
                $tf = true;
				if ( method_exists( $this, $rule ) ) {
					if ( isset( $param ) ) {
						$method = new \ReflectionMethod( $this, $rule );
						$numberOfParams = $method->getNumberOfParameters();
						if ( $numberOfParams < 2 ) {
							throw new Exception\RuntimeException( "Server validation error. Parameter passed via square bracket but this rule doesn\'t accept additional parameter: $rule" );
						} else {
							$tf = $this->$rule( $varName, $param );
						}
					} else {
					    $tf = $this->$rule( $varName );
					}
				} elseif ( array_key_exists( $rule, $this->callbacks ) ) {
					$parsed = isset( $param ) ? $this->callbacks[$rule]( $this->fields[ $varName ], $param ) : $this->callbacks[$rule]( $this->fields[ $varName ], null );
					if ( mb_substr( $rule, 0, 4, 'UTF-8' ) === 'pipe' ) {
						$this->fields[ $varName ] = $parsed;
					} else if ( mb_substr( $rule, 0, 3, 'UTF-8' ) === 'val') {
                        $tf = $parsed;
					}
				} else {
					throw new Exception\RuntimeException( "$varName is supposed to be validated but neither rule nor callback with this name found: $rule" );
				}
                
                /*
                 * record error if rule failed
                 */
                if ( !$tf ) {
                    $err = isset( $this->messages[$rule] ) ? $this->messages[$rule] : _('validation failed');
                    $this->addError( $varName, $err );
                }
			}
		}

		/*
		 * Complete check - after all rules have been parsed
		 */
		return !(bool) count( $this->errors );
	}

    /*
     * Public validation methods
     */
	private function valEmpty( $varName ) {
		$var = $this->fields[$varName];
		return empty( $var );
	}

	private function valNotEmpty( $varName ) {
		$var = $this->fields[$varName];
		return !empty( $var );
	}
	
	private function valEmail( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
		$result = filter_var( $var, FILTER_VALIDATE_EMAIL, array( 'flags' => FILTER_NULL_ON_FAILURE ) );
		return ( $result !== NULL);
	}

	private function valInt( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) && !is_int( $var ) ) {
			return false;
		}
		return (bool) preg_match( '/^[\-+]?[0-9]+$/', $var );
	}

	private function valDecimal( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) && !is_numeric( $var ) ) {
			return false;
		}
		return (bool) preg_match('/^[\-+]?[0-9]+\.[0-9]+$/', $var);
	}
	
	private function valDecimalOrInteger( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) && !is_numeric( $var ) ) {
			return false;
		}
		return (bool) preg_match('/^[\-+]?[0-9]*[.]?[0-9]+$/', $var);
	}

	private function valNumeric( $varName ) {
		$var = $this->fields[$varName];
		return is_numeric( $var );
	}

	private function valNatural( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) && !is_numeric( $var ) ) {
			return false;
		}
		return (bool) preg_match('/^[0-9]+$/', $var);
	}

	private function valNaturalNoZero( $varName ) {
		$var = $this->fields[$varName];
		if ( ( !is_string( $var ) && !is_numeric( $var ) ) || ( $var == 0 ) ) {
			return false;
		}
		return (bool) preg_match('/^[0-9]+$/', $var);
	}

	private function valStr( $varName ) {
		$var = $this->fields[$varName];
		return is_string( $var );
	}
	
	private function valPos( $varName ) {
		$var = $this->fields[$varName];
		return ( is_numeric( $var ) && ( $var > 0 ) );
	}
	
	private function valNeg( $varName ) {
		$var = $this->fields[$varName];
		return  ( is_numeric( $var ) && ( $var < 0 ) );
	}

	private function valBool( $varName ) {
		$var = $this->fields[$varName];
		return is_bool( $var ); 
	}
	
	private function valIp( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
		$result = filter_var( $var, FILTER_VALIDATE_IP, array( 'flags' => FILTER_NULL_ON_FAILURE ) );
		return (bool)( $result !== NULL );
	}
	
	private function valUrl( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
		$result = filter_var( $var, FILTER_VALIDATE_URL, array( 'flags' => FILTER_NULL_ON_FAILURE ) );
		return ( $result !== NULL );
	}
	
	private function valMinStrLength( $varName, $param ) {
		if ( !isset( $param ) || !is_numeric( $param ) ) {
			throw new Exception\RuntimeException( "valMinStrLength - no param or not numeric param" );
		}
		$param = (int)$param;
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
		return !( mb_strlen( $var, 'UTF-8' ) < $param );
	}
	
	private function valMaxStrLength( $varName, $param ) {
		if ( !isset( $param ) || !is_numeric( $param ) ) {
			throw new Exception\RuntimeException( "valMaxStrLength - no param or not numeric param" );
		}
		$param = (int)$param;
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
		return !( mb_strlen($var, 'UTF-8') > $param );
	}
	
	private function valExactStrLength( $varName, $param ) {
		if ( !isset( $param ) || !is_numeric( $param ) ) {
			throw new Exception\RuntimeException( "valExactStrLength - no param or not numeric param" );
		}
		$param = (int)$param;
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
		return ( mb_strlen($var, 'UTF-8') === $param );
	}
	
	private function valMatches( $varName, $param ) {
		if ( !isset( $param ) || !is_string( $param ) ) {
			throw new Exception\RuntimeException( "valMatches. no param or not a string param" );
		}
		return ( $this->fields[$varName] === $this->fields[$param] );
	}

	private function valArray( $varName ) {
		$var = $this->fields[$varName];
		return is_array( $var );
	}
	
	private function valRegex( $varName, $param ) {
		if ( !isset( $param ) || !is_string( $param ) ) {
			throw new Exception\RuntimeException( "valRegex. no param or not a string param" );
		}
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
        return (bool) preg_match( $param, $var );
	}
	
	private function valNumGreaterThan( $varName, $param ) {
		if ( !isset( $param ) || !is_numeric( $param ) ) {
			throw new Exception\RuntimeException( "valNumGreaterThan. no param" );
		}
		$var = $this->fields[$varName];
		return is_numeric( $var ) ? ( $var > $param ) : false;
	}
	
	private function valNumLessThan( $varName, $param ) {
		if ( !isset( $param ) || !is_numeric( $param ) ) {
			throw new Exception\RuntimeException( "valNumLessThan. no param." );
		}
		$var = $this->fields[$varName];
		return is_numeric($var) ? ( $var < $param ) : false;
	}
	
	private function valNumEqualTo( $varName, $param ) {
		if ( !isset( $param ) || !is_numeric( $param ) ) {
			throw new Exception\RuntimeException( "valNumEqualTo. no param." );
		}
		$var = $this->fields[$varName];
		return is_numeric( $var ) ? ( $var == $param ) : false;
	}

	/*
	 * Validates timezone. IMPORTANT. this will validate regular PHP timezones
	 * and also "UTC-12" style. 
	 * if you pass 1 as a parameter - timezones will only be checked for continents.
	 */
	private function valTimezone( $varName, $param = 0 ) {
		$var = $this->fields[$varName];
		$zones = timezone_identifiers_list();
		$error = true;
		
		/*
		 * First - we check regular PHP timezones
		 */
		if ( in_array( $var, $zones) ) {
			if ( $param !== 0 ) {
				$zone = explode('/', $zone); // 0 => Continent, 1 => City
				if ( $zone[0] !== 'Africa' || $zone[0] !==  'America' || $zone[0] !==  'Antarctica' || $zone[0] !==  'Arctic' || $zone[0] !==  'Asia' || $zone[0] !==  'Atlantic' || $zone[0] !==  'Australia' || $zone[0] !==  'Europe' || $zone[0] !==  'Indian' || $zone[0] !==  'Pacific') {
					$error = false;
				}
			}
		/*
		 * Second - we check UTC styletimezones
		 */
		} else if ( 'UTC' === mb_substr( $var, 0, 3, 'UTF-8' ) ) {
			$offsetRange = array ('-12', '-11.5', '-11', '-10.5', '-10', '-9.5', '-9', '-8.5', '-8', '-7.5', '-7', '-6.5',
			'-6', '-5.5', '-5', '-4.5', '-4', '-3.5', '-3', '-2.5', '-2', '-1.5', '-1', '-0.5', '+0', '+0.5', '+1',
			'+1.5', '+2', '+2.5', '+3', '+3.5', '+4', '+4.5', '+5', '+5.5', '+5.75', '+6', '+6.5', '+7', '+7.5', '+8',
			'+8.5', '+8.75', '+9', '+9.5', '+10', '+10.5', '+11', '+11.5', '+12', '+12.75', '+13', '+13.75', '+14');
			$time = mb_substr($var, 3, mb_strlen($var), 'UTF-8');
			if ( !in_array( $time, $offsetRange ) ) {
				$error = false;
			}
		/*
		 * Third - validation failed.
		 */
		} else {
			$error = false;
		}
		
	    return $error;	
	}

	/**
	 * Validating string - checking if only alpha values
	 */
	private function valAlpha( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
		return (bool) preg_match( '/^[a-z]+$/i', $var );
	}

	/**
	 * Validating string - checking if only alphanumeric values
	 */
	private function valAlphaNum( $varName ) {
		$var = $this->fields[$varName];
		if ( !is_string( $var ) ) {
			return false;
		}
		return (bool) preg_match( '/^[a-z0-9]+$/i', $var );
	}

	/**
	 * Empty filler validation method. Does nothing.
	 */
	private function valFiller( $varName ) {
        return true;
	}

	/**
	 * Validating string - making sure it's in allowed selection of values.
	 */
	private function valStrInRange( $varName, $param ) {
		$var = $this->fields[$varName];
		if ( !isset( $param ) || $param === '' || !is_string( $param ) ) {
			throw new Exception\RuntimeException( "valStrInRange. mo param or invalid param." );
		}
		if ( !is_string( $var ) ) {
			return false;
		}
		$range = explode( '::', $param );
		if ( count( $range ) > 0 ) {
			return in_array( $var, $range );
		} else {
			throw new Exception\RuntimeException( "valStrInRange. Invalid range, count 0." );
		}
	}

	/**
	 * Another variable must be present (though at this point it's unknown if passed validation).
     * Otherwise - validation fails.
	 */
	private function mustExist( $varName, $param ) {
		$var = $this->fields[ $varName ];
		if ( !isset( $param ) || $param === '' || !is_string( $param ) ) {
			throw new Exception\RuntimeException( "mustExist. param is not a string or empty string." );
		}
        return array_key_exists( $param, $this->fields );
	}

	/**
	 * copyToSet - special case 
	 */
	private function copyToSet( $varName, $param ) {
		if ( !isset( $param ) || !is_string( $param ) ) {
			throw new Exception\RuntimeException( "mustExist. no param or not a string" );
		}
		$var = $this->fields[$varName];
        $data = explode( '::', $param );
        $content = ( count( $data ) === 2 ) ? array( $var, $data[1] ) : $var;
        $copyTo =  ( count( $data ) === 2 ) ? $data[0] : $param;

        /*
         * Creating a set if not yet exists
         */
        if ( !array_key_exists( $copyTo, $this->sets ) || !is_array( $this->sets[$copyTo] ) ) {
            $this->sets[$copyTo] = array();
        }
        
		/*
		 * Copying to set
		 */
        $this->sets[$copyTo][$varName] = $content;
        return true;
	}
}
